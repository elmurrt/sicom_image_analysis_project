import numpy as np
from src.forward_model import CFA
from src.methods.ouahmane_fatima_ezzahra.naive_interpolation import  naive_interpolation
from src.methods.ouahmane_fatima_ezzahra.lmmse import  lmmse_color_smooth_demosaicking
from src.methods.ouahmane_fatima_ezzahra.ahd import  ahd_demosaicking

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
   
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)
   
    demosaicked_image_naive = naive_interpolation(op, y)
    demosaicked_image_lnmse = lmmse_color_smooth_demosaicking(op, y)
    demosaicked_image_ahd = ahd_demosaicking(op, y)

    return [demosaicked_image_naive, demosaicked_image_lnmse, demosaicked_image_ahd]
